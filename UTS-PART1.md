- Part 1
    - II Buatlah
        - b. Program roda_to_robot
        Letak program roda_to_robot.py ada di ./UTS/part1/part_1_II_b.py.
        Program dapat dijalankan dengan 
        
        > cd UTS/part1
        > python part_1_II_b.py
        - c. Program roda_to_pose
        Letak program roda_to_pose.py ada di ./UTS/part1/part_1_II_c.py.
        Program dapat dijalankan dengan 
        
        > cd UTS/part1
        > python part_1_II_c.py
    - III
        Buka 2 terminal yang environmentnya sudah diaktifkan dengan
        > source ./devel/setup.bash
        
        Jalankan perintah 'roslaunch gazebo_ros_empty_world.launch' pada terminal 1
        
        Jalankan perintah 'roslaunch m2wr spawn.launch' pada terminal 2
        
        Jalankan perintah 'rosrun m2wr node_roda_to_pose.py' pada terminal 2
        
        Jalankan perintah 'rosrun m2wr node_roda_to_robot.py' pada terminal 2 
     
    -IV
        Buka 5 terminal yang environmentnya sudah diaktifkan dengan
        
        > source ./devel/setup.bash
        
        Jalankan perintah 'roslaunch gazebo_ros empty_world.launch' pada terminal 1
        
        Jalankan perintah 'roslaunch m2wr spawn.launch' pada terminal 2
        
        Jalankan perintah 'roslaunch m2wr rviz.launch' pada terminal 2
        
        Jalankan perintah 'rosrun m2wr marker_node.py' pada terminal 3
        
        Jalankan perintah 'rosrun m2wr get_position.py' pada terminal 4
        
        Jalankan perintah 'rosrun m2wr move_by_tire_seq.py' pada terminal 5

