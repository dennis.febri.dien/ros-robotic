#!/usr/bin/env python

import rospy
from std_msgs.msg import String

def roda_to_robot(x, y, theta, w_l, w_r, t, l, r):
    v_l = w_l * r
    v_r = w_r * r
    v = (v_r + v_l)/2
    w = (v_r - v_l)/l
    return (v, w)


if __name__ == "__main__":
    pub = rospy.Publisher('roda_to_robot', String, queue_size=10)
    rospy.init_node('pub_roda_to_robot', anonymous=True)
    output = "linear speed : %s, angular speed: %s" % roda_to_robot(40, 20, 0, 2, 3, 10, 10.6, 1)
    pub.publish(output)
    print(output)
