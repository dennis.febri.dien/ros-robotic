#!/usr/bin/env python

from signal import signal, SIGINT
from sys import exit

import numpy as np

import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from node_roda_to_robot import roda_to_robot 
from node_roda_to_pose import roda_to_pose
import math
from gazebo_msgs.msg import LinkStates

from extended_kalman_filter import (
    calc_input, observation, observation_model,
    ekf_estimation, plot_covariance_ellipse
)
import matplotlib.pyplot as plt

import tf
# Estimation parameter of EKF
Q = np.diag([1.0, 1.0])**2  # Observation x,y position covariance
R = np.diag([0.1, 0.1, np.deg2rad(1.0), 1.0])**2  # predict state covariance

#  Simulation parameter
Qsim = np.diag([0.5, 0.5])**2
Rsim = np.diag([1.0, np.deg2rad(30.0)])**2

DT = 0.1  # time tick [s]
SIM_TIME = 50.0  # simulation time [s]

show_animation = True


PI = math.pi
R_RODA = 0.1
L_ROBOT = 0.3

class M2WR(object):
    def __init__(self):
        self.value = 0
        self.lin_speed = 0.5
        self.ang_speed = 0.5
        self.time = 1
        self.time_k = 0.0

        # State Vector [x y yaw v]'
        self.xEst = np.zeros((4, 1))
        self.xTrue = np.zeros((4, 1))
        self.PEst = np.eye(4)

        self.xDR = np.zeros((4, 1))  # Dead reckoning
        self.y = 0
        # history
        self.hxEst = self.xEst
        self.hxTrue = self.xTrue
        self.hxDR = self.xTrue
        self.hz = np.zeros((1, 2))
        self.name_robot = "m2wr::link_chassis"
        self.name_landmark = "landmark1::my_box" 
        self.link_pose_robot = None
        self.link_pose_landmark = None 
        rospy.init_node('m2wr_controller')
        self.sensor_subscriber =  rospy.Subscriber("/gazebo/link_states", LinkStates, self.callback)
        self.velocity_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=10)

    def callback(self, data):
        try:
            ind = data.name.index(self.name_robot)
            self.link_pose_robot = data.pose[ind]
            ind = data.name.index(self.name_landmark)
            self.link_pose_landmark = data.pose[ind]
            #(r, p, y) = tf.transformations.euler_from_quaternion([
            #    self.link_pose_robot['orientation'].x, self.link_pose_robot['orientation'].y,
            #    self.link_pose_robot['orientation'].z, self.link_pose_robot['orientation'].w
            #])

            #self.y = y

        except ValueError:
            pass


    def run(self):
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            vel_msg = Twist()
            vel_msg.linear.x = self.lin_speed
            vel_msg.linear.y = 0
            vel_msg.linear.z = 0
            vel_msg.angular.x = 0
            vel_msg.angular.y = 0
            vel_msg.angular.z = self.ang_speed
            print("lin_speed : ",vel_msg.linear.x)
            print("ang_speed : ",vel_msg.angular.z)
            print(self.link_pose_robot)


            u = np.array([[self.lin_speed, self.ang_speed]]).T

            self.xTrue, z, self.xDR, ud = observation(self.xTrue, self.xDR, u)

            self.xEst, self.PEst = ekf_estimation(self.xEst, self.PEst, z, ud)

            # store data history
            self.hxEst = np.hstack((self.hxEst, self.xEst))
            self.hxDR = np.hstack((self.hxDR, self.xDR))
            self.hxTrue = np.hstack((self.hxTrue, self.xTrue))
            self.hz = np.vstack((self.hz, z))

            if show_animation:
                plt.cla()
                plt.plot(self.hz[:, 0], self.hz[:, 1], ".g")
                plt.plot(self.hxTrue[0, :].flatten(),
                        self.hxTrue[1, :].flatten(), "-b")
                plt.plot(self.hxDR[0, :].flatten(),
                        self.hxDR[1, :].flatten(), "-k")
                plt.plot(self.hxEst[0, :].flatten(),
                        self.hxEst[1, :].flatten(), "-r")
                plot_covariance_ellipse(self.xEst, self.PEst)
                plt.axis("equal")
                plt.grid(True)
                plt.pause(0.001)

            t0 = rospy.Time.now().to_sec()

            while(rospy.Time.now().to_sec() - t0 < self.time):
                self.velocity_publisher.publish(vel_msg)

            vel_msg.linear.x = 0
            vel_msg.angular.z = 0
            self.velocity_publisher.publish(vel_msg)
            r.sleep()

def handler(signal_received, frame):
    # Handle CTRL-C in Python2
    print("")
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)


if __name__ == '__main__':
    signal(SIGINT, handler)
    topic = raw_input("Topic: ")
    robot = M2WR()
    print("Robot start run")
    try:
        robot.run()
    except rospy.ROSInterruptException: 
        pass
