#!/usr/bin/env python

from signal import signal, SIGINT
from sys import exit

import rospy
from geometry_msgs.msg import Twist
from node_roda_to_robot import roda_to_robot 
from node_roda_to_pose import roda_to_pose
import math

PI = math.pi    

class M2WR :
    def __init__(self, topic):
        self.topic = topic

    def move(self, lin_speed, ang_speed, time):
        velocity_publisher = rospy.Publisher(self.topic, Twist, queue_size=10)
        vel_msg = Twist()
        
        vel_msg.linear.x = lin_speed
        vel_msg.linear.y = 0
        vel_msg.linear.z = 0
        vel_msg.angular.x = 0
        vel_msg.angular.y = 0
        vel_msg.angular.z = ang_speed
        
        while not rospy.is_shutdown():

            t0 = rospy.Time.now().to_sec()

            while(rospy.Time.now().to_sec() - t0 < time):
                velocity_publisher.publish(vel_msg)

            vel_msg.linear.x = 0
            vel_msg.angular.z = 0
            velocity_publisher.publish(vel_msg)
                
            break

def handler(signal_received, frame):
    # Handle CTRL-C in Python2
    print("")
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)

R_RODA = 0.1
L_ROBOT = 0.3

if __name__ == '__main__':
    signal(SIGINT, handler)
    rospy.init_node('m2wr_controller', anonymous=True)
    topic = raw_input("Topic to control : ")
    robot = M2WR(topic)
    (x,y,theta) = (0,0,0)
    w_l_list = [0.3, 0.6, -0.6, 0.5, 0.6, 4, -0.6, -3, -0.3, -0.5]
    w_r_list = [0.5, 0.6, -0.6, 0.3, -0.6, 4, 0.6, -3, -0.5, -0.3]
    t = [6, 6, 6, 6, 6, 6, 6, 6, 10, 10]
    try:
        for i in range(len(t)):
            omega_left = w_l_list[i]
            omega_right = w_r_list[i]
            time = t[i]
            (lin_speed, ang_speed) = roda_to_robot(x, y, theta, omega_left, omega_right, time, L_ROBOT, R_RODA)
            (x,y,theta) = roda_to_pose(x, y, theta, omega_left, omega_right, time, L_ROBOT, R_RODA)
            print(x,y,theta)
            robot.move(lin_speed, ang_speed, time)
            rospy.sleep(10)
    except rospy.ROSInterruptException: 
        pass
