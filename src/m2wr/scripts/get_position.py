#!/usr/bin/env python

from signal import signal, SIGINT
from sys import exit

import rospy
from gazebo_msgs.srv import GetLinkState 
from gazebo_msgs.msg import LinkState


def getPositionRobot():
    rospy.wait_for_service('/gazebo/get_link_state')
    try:
        res = model_info_prox = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)
        print(res("m2wr::link_chassis",""))
        #print(dir(res))
        #print(type(res))
    except rospy.ServiceException, e:
        print "Service call failed: %s" %e



def handler(signal_received, frame):
    # Handle CTRL-C in Python2
    print("")
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)

if __name__ == '__main__':
    signal(SIGINT, handler)
    rospy.init_node('m2wr_get_position', anonymous=True)
    try:
        while True:
            getPositionRobot()
            rospy.sleep(0.5)
    except rospy.ROSInterruptException:
        pass
    

#model_info_prox = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState)
#rospy.wait_for_service('/gazebo/get_link_state')
#print "Link 7 Pose:"    , endl , model_info_prox( "lbr4_allegro::lbr4_7_link" , "world" )
