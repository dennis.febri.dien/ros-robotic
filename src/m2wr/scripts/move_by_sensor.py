#!/usr/bin/env python

from signal import signal, SIGINT
from sys import exit

import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from node_roda_to_robot import roda_to_robot 
from node_roda_to_pose import roda_to_pose
import math

PI = math.pi    

class M2WR :
    def __init__(self, topic):
        self.topic = topic

    def move(self, lin_speed, ang_speed, time):
        velocity_publisher = rospy.Publisher(self.topic, Twist, queue_size=10)
        vel_msg = Twist()
        
        vel_msg.linear.x = lin_speed
        vel_msg.linear.y = 0
        vel_msg.linear.z = 0
        vel_msg.angular.x = 0
        vel_msg.angular.y = 0
        vel_msg.angular.z = ang_speed
        
        while not rospy.is_shutdown():

            t0 = rospy.Time.now().to_sec()

            while(rospy.Time.now().to_sec() - t0 < time):
                velocity_publisher.publish(vel_msg)

            vel_msg.linear.x = 0
            vel_msg.angular.z = 0
            velocity_publisher.publish(vel_msg)
                
            break

def handler(signal_received, frame):
    # Handle CTRL-C in Python2
    print("")
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)

def callback(msg):
    front = msg.ranges[360]
    left = msg.ranges[0]
    right = msg.ranges[719]
    print(left, front, right)
    return(left, front, right)


R_RODA = 0.1
L_ROBOT = 0.3

if __name__ == '__main__':
    signal(SIGINT, handler)
    rospy.init_node('m2wr_controller', anonymous=True)
    topic = raw_input("Topic: ")
    robot = M2WR(topic)
    (x,y,theta) = (0,0,0)
    sub = rospy.Subscriber('/laser/scan', LaserScan, callback)
    print(dir(sub))
    try:
        while True:
            lin_speed = input("Input Linear Speed : ")
            ang_speed = input("Input Angular Speed : ")
            time = input("Input time : ")
            robot.move(lin_speed, ang_speed, time)
    except rospy.ROSInterruptException: 
        pass
