#!/usr/bin/env python

from signal import signal, SIGINT
from sys import exit

import rospy
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from node_roda_to_robot import roda_to_robot 
from node_roda_to_pose import roda_to_pose
import math

PI = math.pi
R_RODA = 0.1
L_ROBOT = 0.3

class M2WR(object):
    def __init__(self):
        self.value = 0
        self.lin_speed = 0
        self.ang_speed = 0
        self.time = 0
        rospy.init_node('m2wr_controller')
        self.sensor_subscriber = rospy.Subscriber('/laser/scan', LaserScan, self.callback)
        self.velocity_publisher = rospy.Publisher('cmd_vel', Twist, queue_size=10)
        
    def callback(self, msg):
        front = msg.ranges[360]
        left = msg.ranges[0]
        right = msg.ranges[719]
        #print(left, front, right)
        if(front > 2):
            (lin_speed, ang_speed) = roda_to_robot(0, 0, 0, 11, 11, 0.2, L_ROBOT, R_RODA)
            self.ang_speed = ang_speed
            self.lin_speed = lin_speed
            self.time = 0.2
        else:
            if(left > right):
                (lin_speed, ang_speed) = roda_to_robot(0, 0, 0, 0, 5, 1 , L_ROBOT, R_RODA)
                self.ang_speed = ang_speed
                self.lin_speed = lin_speed
                self.time = 1
            elif(left < right):
                (lin_speed, ang_speed) = roda_to_robot(0, 0, 0, 5, 0, 1 , L_ROBOT, R_RODA)
                self.ang_speed = ang_speed
                self.lin_speed = lin_speed
                self.time = 1
            else:
                (lin_speed, ang_speed) = roda_to_robot(0, 0, 0, 11, 11, 0.2, L_ROBOT, R_RODA)
                self.ang_speed = ang_speed
                self.lin_speed = lin_speed
                self.time = 1


    def run(self):
        r = rospy.Rate(10)
        while not rospy.is_shutdown():
            vel_msg = Twist()
            vel_msg.linear.x = self.lin_speed
            vel_msg.linear.y = 0
            vel_msg.linear.z = 0
            vel_msg.angular.x = 0
            vel_msg.angular.y = 0
            vel_msg.angular.z = self.ang_speed
            print("lin_speed : ",vel_msg.linear.x)
            print("ang_speed : ",vel_msg.angular.z)

            t0 = rospy.Time.now().to_sec()

            while(rospy.Time.now().to_sec() - t0 < self.time):
                self.velocity_publisher.publish(vel_msg)

            vel_msg.linear.x = 0
            vel_msg.angular.z = 0
            self.velocity_publisher.publish(vel_msg)
            r.sleep()

def handler(signal_received, frame):
    # Handle CTRL-C in Python2
    print("")
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)


if __name__ == '__main__':
    signal(SIGINT, handler)
    topic = raw_input("Topic: ")
    robot = M2WR()
    print("Robot start run")
    try:
        robot.run()
    except rospy.ROSInterruptException: 
        pass
