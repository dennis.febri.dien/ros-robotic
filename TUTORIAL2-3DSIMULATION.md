# This is Documentation for Tutorial 2

(1) To modify both chassis and wheels, open m2wr.xacro in your preferred text editor.
You can change the color of chassis, left wheel, or right wheel (individually) by replacing its name, e.g: as you can see in 1.jpg, we change line 37 from Gazebo/Blue to Gazebo/Green to distinguish between left and right wheel. Then, if you wanna adjust the size of chassis, you can set the value of size in box tag <box> to whatever you want. This time, we make our chassis to be 1.5 bigger (compare line 52 and 57 to see the difference). Lastly, to alter the position of either wheels, we replace the xyz value in origin tag <origin> of respective joint tag <joint>. To make it discernible, the wheels are set to be apart from each other.

(3) There are some steps to activate the marker. Please follow the instruction below:
- Every Open the terminal, Make sure to change environment with this command
(assume you are in the directory)
'source devel/setup.bash'
- Then, run this command to open GAZEBO empty world
'roslaunch gazebo_ros empty_world.launch'
- Then open another terminal and change env, then launch spawn with this command
'roslaunch m2wr spawn.launch'
- Then Open Rviz.launch with this command
'roslaunch m2wr rviz.launch'
- Then Run script simple
'rosrun m2wr simple_move.py'
and then add topic :
cmd_vel
wait to the next step

- Then Run Script marker with this command
'rosrun m2wr marker_node.py'
and then add topic :
cmd_vel

-then back to the cmd simple_move
fill the parameter

-check rviz for program works

(2) We assume you have to do number (3), so let's continue
- CTRL+C in simple_move script.
- run this command instead
'rosrun m2wr u_turn.py'

-fill the parameter and follow the instruction
-check rviz for program works
