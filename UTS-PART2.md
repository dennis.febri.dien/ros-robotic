Untuk menjalankan program, Lakukan Hal-hal berikut :
1. Buka 2 buah terminal dan aktifkan environment dengan perintah :
source ./devel/setup.bash (pastikan file-file telah di chmod (+x) )
2. Jalankan perintah untuk menampilkan map:
roslaunch m2wr circuit.launch
3. Jalankan perintah untuk menampilkan robot:
roslaunch m2wr part2.launch
4. Jalankan perintah untuk menggerakkan robot:
rosrun m2wr simple_move_modified.py (masukan topic cmd_vel)