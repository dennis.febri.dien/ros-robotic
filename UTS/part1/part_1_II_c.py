from math import sin,cos
def roda_to_pose(posisi_x, posisi_y, sudut_robot, kec_sudut_roda_l, kec_sudut_roda_r, durasi, jarak_roda, jari):
	#mendapatkan kecepatan linear kiri dan kanan
	kec_linear_l = kec_sudut_roda_l * jari
	kec_linear_r = kec_sudut_roda_r * jari
	
	#jika lurus
	if (kec_linear_l == kec_linear_r):
		sudut_akhir_robot = sudut_robot
		posisi_x_akhir = posisi_x + kec_linear_l * durasi * cos(sudut_robot)
		posisi_y_akhir = posisi_y + kec_linear_l * durasi * sin(sudut_robot)
	#lingkaran
	else:
	#hitung radius
		R = jarak_roda/2.0 * ((kec_linear_l + kec_linear_r) / (kec_linear_r - kec_linear_l))
		#hitung icc
		ICC_x = posisi_x - R * sin(sudut_robot)
		ICC_y = posisi_y + R * cos(sudut_robot)
		#hitung kec sudut
		omega = (kec_linear_r - kec_linear_l) / jarak_roda
		#hitung perubahan sudut
		dtheta = omega * durasi
		#forward kinematic u/ diff drive
		posisi_x_akhir = cos(dtheta) * (posisi_x-ICC_x) - sin(dtheta) * (posisi_y-ICC_y) + ICC_x
		posisi_y_akhir = sin(dtheta) * (posisi_x-ICC_x) + cos(dtheta) * (posisi_y-ICC_y) + ICC_y
		sudut_akhir_robot = sudut_robot + dtheta
	return posisi_x_akhir, posisi_y_akhir, sudut_akhir_robot

if __name__ == "__main__":
	print(roda_to_pose(40, 20, 0, 2, 3, 10, 10.6, 1))